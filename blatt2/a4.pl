/*
Aufgabenblatt 2
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

sohn(m2,m1,w1).
sohn(m4,m3,w3).
sohn(m5,m2,w4).
sohn(m6,m4,w2).
sohn(m7,m4,w4).
sohn(m8,m4,w4).

tochter(w2,m1,w1).
tochter(w4,m3,w3).
tochter(w5,m2,w4).
tochter(w6,m2,w4).
tochter(w7,m4,w2).
tochter(w8,m5,w7).

/*
sohn(S,V,M) = S ist ein Sohn seines Vaters V und seiner Mutter M
tochter(T,V,M) = T ist eine Tochter ihres Vaters V und ihrer Mutter M
*/

% Gibt es entweder Vater oder Mutter von S so ist S ein Sohn der Eltern
sohn(S,E) :- sohn(S,E,_).
sohn(S,E) :- sohn(S,_,E).

% Gibt es entweder Vater oder Mutter von T so ist T eine Tochter der
% Eltern (E)
tochter(T,E) :- tochter(T,E,_).
tochter(T,E) :- tochter(T,_,E).


/*
a)
Definieren Sie mit Hilfe der Prädikate sohn(S,V,M) und tochter(Z,V,M)
folgende Eigenschaften von Personen:

weiblich(X) % X ist weiblich
maennlich(X) % X ist maennlich
*/

weiblich(X) :- tochter(X,_,_). % ist X eine Tochter -> weiblich
weiblich(X) :- tochter(_,_,X). % ist X eine Mutter -> weiblich

maennlich(X) :- sohn(X,_,_). % ist X ein Sohn -> maennlich
maennlich(X) :- sohn(_,X,_). % ist X ein Sohn -> maennlich

/*
b)
folgende Beziehungen zwischen Personen:
schwester(X,Y) % X ist eine Schwester von Y
bruder(X,Y) % X ist ein Bruder von Y
*/

% X ist Schwester von Y wenn sie ein Kind ist (also Tochter) und die
% gleichen Eltern (V und M) ein anderes Kind (Sohn oder Tochter) haben
schwester(X,Y) :- not(X=Y), tochter(X,V,M), tochter(Y,V,M).
schwester(X,Y) :- not(X=Y), tochter(X,V,M), sohn(Y,V,M).

% wie bei Schwester nur muss X Sohn der Eltern sein
bruder(X,Y) :- not(X=Y), sohn(X,V,M), sohn(Y,V,M).
bruder(X,Y) :- not(X=Y),sohn(X,V,M), tochter(Y,V,M).

% geschwister für spätere Nutzung
geschwister(X,Y) :- schwester(X,Y).
geschwister(X,Y) :- bruder(X,Y).

/*
(Benutzen Sie not(X=Y) im Rumpf einer Regel, um auszudrücken, daß X und
Y nicht durch u denselben Term belegt sein sollen.)

nichte(X,Y) % X ist Tochter einer Schwester oder eines Bruders von Y
neffe(X,Y) % X ist Sohn einer Schwester oder eines Bruders von Y
*/

nichte(X,Y) :- geschwister(Y,Z), tochter(X,Z).
neffe(X,Y) :- geschwister(Y,Z), sohn(X,Z).

/*
c)
kind(X,Y) % X ist ein Kind von Y
nachkomme(X,Y) % X ist ein Nachkomme von Y
*/
% Ist K ein Sohn oder Tochter von E so ist es ein Kind von E.
kind(K,E) :- sohn(K,E).
kind(K,E) :- tochter(K,E).

nachkomme(X,Y) :- kind(X,Y).
nachkomme(X,Y) :- nachkomme(X,Z), kind(Z,Y).

/*
d)

[trace]  ?- nachkomme(w8,Y).
   Call: (6) nachkomme(w8, _G413) ? creep
   Call: (7) kind(w8, _G413) ? creep
   Call: (8) sohn(w8, _G413) ? creep
   Call: (9) sohn(w8, _G413, _G484) ? creep
   Fail: (9) sohn(w8, _G413, _G484) ? creep
   Redo: (8) sohn(w8, _G413) ? creep
   Call: (9) sohn(w8, _G483, _G413) ? creep
   Fail: (9) sohn(w8, _G483, _G413) ? creep
   Fail: (8) sohn(w8, _G413) ? creep
   Redo: (7) kind(w8, _G413) ? creep
   Call: (8) tochter(w8, _G413) ? creep
   Call: (9) tochter(w8, _G413, _G484) ? creep
   Exit: (9) tochter(w8, m5, w7) ? creep
   Exit: (8) tochter(w8, m5) ? creep
   Exit: (7) kind(w8, m5) ? creep
   Exit: (6) nachkomme(w8, m5) ? creep
Y = m5


?- nachkomme(w8,Y).
Y = m5 ;
Y = w7 ;
Y = m2 ;
Y = w4 ;
Y = m4 ;
Y = w2.
*/

kante(1,2).
kante(1,3).
kante(2,4).
kante(2,5).
kante(3,4).
kante(4,7).
kante(5,6).
kante(6,7).

weg1(X,Y):-kante(X,Y).
weg1(X,Y):-kante(X,Z),weg1(Z,Y).

weg2(X,Y):-kante(X,Y).
weg2(X,Y):-weg2(X,Z),kante(Z,Y).

weg3(X,Y):-kante(X,Z),weg3(Z,Y).
weg3(X,Y):-kante(X,Y).

weg4(X,Y):-weg4(X,Z),kante(Z,Y).
weg4(X,Y):-kante(X,Y).

/*
Aufgabenblatt 7
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

/*
 * A13)
 * vars(+Term,-Variables)
 * Output should be list containing prolog variables found in term
 */

vars(Term, Variables) :- vars(Term, [], Variables).
vars(Term, _, [Term]) :- var(Term). % Ist Term eine Variable -> fertig
vars([Term], _, [Term]) :- var(Term). % Steckt Variable in einer Liste
% ist das auch OK. Beim Splitten von Listen ([Head|Tail]) ist TAIL
% *IMMER* eine Liste. Deshalb prüfen wir das hier mal so :p
vars([Term], Sammlung, Variables) :- Term =.. [_|TermArgs],  % Zerlegung
	vars(TermArgs, Sammlung, Variables).
vars(Term, Sammlung, Variables) :- Term =.. [_|TermArgs],  % Zerlegung
	vars(TermArgs, Sammlung, Variables).

%moepi:
%vars(+Term,-Variablen).
varsm(Term,Variablen):-varsm(Term,[],Variablen).

/*
%erster ansatz - vorerst verworfen, aber mal drin gelassen
%vars(+Term,+Sammlung,-Variablen).
varsm([],[X],[X]):-!.
varsm([],[],[]).
varsm(X,[],[X]):-var(X),!.
%varsm(var(X),[],[X]). %kuerzer, aber bringt foobar output
varsm([X|Term],S,V):-varsm(X,S,Vs),varsm(Term,Vs,V),!.
varsm(Term,S,V):-Term =.. [_|Args],varsm(Args,S,V),!.
%varsm([],[],[]):-!.
*/

%neuer, strukturierterer ansatz
/*
var(Variable,Sammlung,Variablen+Sammlung)
var([],Sammlung,Sammlung)
var(Liste,Sammlung,Variablen) = var(Kopf,Sammlung,Variablen) + var(RestListe,Variablen,MehrVariablen)
*/

%vars(+Term,-Variablen).
varm(Term,Variablen):-varm(Term,[],Variablen).

%vars(+Term,+Sammlung,-Variablen).
varm(Var,Sammlung,Variablen):-(var(Var),!,Variablen=[Var|Sammlung]);(Var=..[Fun|Args],varm(Args,Sammlung,Variablen)). %die regel ist crap, weil endless
%hier fehlt eine entscheidung. pruefen ob liste, wenn ja, dann untere regeln, sonst obere regel (nur atomarer var(Var)-part) und sonst zerlegung in funktor und argliste.. aber grad kP wie
varm([],Sammlung,Sammlung).
varm([Kopf|Restliste],Sammlung,Variablen) :- varm(Kopf,Sammlung,NeueSammlung),varm(Restliste,NeueSammlung,Variablen).



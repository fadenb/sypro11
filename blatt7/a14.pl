/*
Aufgabenblatt 7
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

/*
 * A14)
 * weg(+Punkt,+Punkt)
 */

kante(1,2).
kante(1,3).
kante(2,4).
kante(2,5).
kante(3,4).
kante(4,7).
kante(5,6).
kante(6,7).
kante(7,5).

weg(X,Y) :- weg(X,Y,[]).
weg(X,Y,Pfad) :- kante(X,Y),checkInPfad(X,Y,Pfad).
weg(X,Y,Pfad) :- kante(X,Z), weg(Z,Y,[[X,Z]|Pfad]).
%prueft ob kante(X,Y) in (kanten-)Liste Pfad enthalten ist
%w8.. haette nicht ein ! schon gereicht, dann koennt ich mir das unten und das checken sparen :D
%ahh mit ! (hinter den zeile 23 und 24 gehts ja nicht, weil er dann das ergebnis beschraenkt. muss anders gehen!
%=> loesung fuer a reicht ein ! also :
/*
weg(X,Y) :- kante(X,Y),!.
weg(X,Y) :- kante(X,Z), weg(Z,Y),!.
*/
%fuer loesung zu c muss mit pfadliste gearbeitet werden. muss in uebung, cya!
checkInPfad(_,_,[]).
checkInPfad(X,Y,[Node|Pfad]):-Node=[X,Y],fail | checkInPfad(X,Y,Pfad).

doppelkante(X,Y):-kante(X,Z),kante(Z,Y),write(X),write('->'),write(Z),write('->'),write(Y),nl,fail.

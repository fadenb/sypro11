/*
Aufgabenblatt 5
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

/*
 * A10)
 */
%knoten(L,R) :- baum(L), baum(R).
baum(blatt(N)) :- number(N).
baum(knoten(X,Y)) :- baum(X), baum(Y).

/*
 * A10-a-i)
 */
verzweigungen(blatt(N),0) :- number(N). % 0 Verzweigungen falls Baum nur blatt mit Nummer ist
verwzeigungen(knoten(X,Y),N) :- verzweigungen(X,NL), verzweigungen(Y,NR), N is NL + NR.


/*
Aufgabenblatt 5
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/
/*
 * A9-a)
 *
 * suffixe(+Liste,-Suffixe)
 */

suffixe(L,O) :- suffixe1(L,X), append([L],X,O).
% Das das mit append unschön ist ist mir bewusst. Aber
% ich bin gerade unkreativ und es funktioniert ;)
suffixe1([],[]).
suffixe1([_|R],[R|L]) :- suffixe1(R,L).

/*
 * rekursiv ist [a,b,c],[b,c],[c] 'leichter'
 */

/*
 * A9-b)
 * dritte(+Liste,-JedesDritteElement)
 */

dritte([],[]) :- !.
dritte([_],[]) :- !.
dritte([_,_],[]) :- !.
dritte([_,_,D|L],X) :- dritte(L,Y),append([D],Y,X).

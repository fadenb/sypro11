/* Implementierung eines Grphen
   und einer Wegdefinition
*/


weg(X,Y) :- kante(X,Y).
weg(X,Y) :- kante(X,Z), weg(Z,Y).


kante(1, 2). % Kanten des Graphen
kante(1, 3).
kante(2, 4).
kante(2, 5).
kante(3, 4).
kante(4, 7).
kante(5, 6).
kante(6, 7).

/* Aufgabe: Definiere ein Praedikat aehnlich/2 zwischen Punkten des Graphen, das besagt:
	Zwei Punkte X und Y sind aehnlich,
	1. wenn sie vom selben Punkt durch eine Kate erreicht werden koennen, d.h. wenn es einen Punkt Z gibt und Kanten von Z nach X und Z nach Y.
	2. wenn es zwei aehnliche Punkte X' und Y' gibt und eine Kante von X' zu X und eine Katen von Y' zu Y fuehrt.

	(mindestens folgende Paare sollten aehnlich sein: 2~3,5~4,4~5,4~4)

*/

aehnlich(X,Y) :- kante(Z,X), kante(Z,Y).
aehnlich(X,Y) :- kante(X1, X), kante(Y1, Y), aehnlich(X1,Y1).



/*
Anstelle von aehnlich(X,Y). und oft ; tippen kann man auch folgendes
aufrufen: setof((X,Y),aehnlich(X,Y),Set).
Dies zeigt sets für die das praedikat aehnlich erfuellt ist
*/

/* tracer Beispiel fuer hausaufgabe:
trace(aehnlich/2,[call,redo,exit]). <-- traced praedikat aehnlich bei
call,redo und exit
Dann einfach "aehnlich(2,3)." oder so aufrufen um trace zu sehen.

nodebug. schaltet es wieder ab.
*/

/* protocol(dateiname). speichert session in file bis noprotocol. aufgerufen wird */


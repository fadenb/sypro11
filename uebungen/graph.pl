/* Implementierung eines Graphen
   und einer Wegdefinition
*/


weg(X,Y) :- kante(X,Y).
weg(X,Y) :- kante(X,Z), weg(Z,Y).


kante(1, 2). % Kanten des Graphen
kante(1, 3).
kante(2, 4).
kante(2, 5).
kante(3, 4).
kante(4, 7).
kante(5, 6).
kante(6, 7).


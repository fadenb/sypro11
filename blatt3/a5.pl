/*
Aufgabenblatt 3
Symbolisches Programmieren WiSe 11/12
Tristan Helmich
*/

% Definition natürlicher Zahlen aus Vorlesung
nat(0).
nat(s(X)) :- nat(X).

% Definition der Addition auf unseren Zahlen
add(X,0,X) :- nat(X). % X addiert zu 0 ergibt X falls X eine unserer Zahlen ist
add(X,s(Y),s(Z)) :- add(X,Y,Z).

% maximum zweier zahlen X und Y ist Z
max(0,Y,Y) :- nat(Y). % alles größer als 0
max(s(X),0,s(X)) :- nat(x). % Nachfolger einer unserer Zahlen muss auch größer 0 sein
max(s(X),s(Y),s(Z)) :- max(X,Y,Z). % Rückführen von anderen Fällen auf die vorherigen

/*
5a)
Subtrahiere X von Y
Y-X = Z falls Y >= X sonst Z = 0
*/
sub(X,0,X) :- nat(X).
sub(0,Y,0) :- nat(Y).
sub(X,s(Y),Z) :- X=s(A), sub(A,Y,Z). % Entfern bei jedem "Durchlauf" eine Nachfolger Klammerung um X


/*
5b)
Multipliziere X und Y. Ergebnis in Z.
X * 0 = 0
(X * (Y + 1)) = ((X * Y) + X)
*/

mult(X,0,0) :- nat(X).
mult(X,s(Y),Z) :- mult(X,Y,A), add(X,A,Z).




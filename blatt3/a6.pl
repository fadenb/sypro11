/*
Aufgabenblatt 3
Symbolisches Programmieren WiSe 11/12
Tristan Helmich
*/

/*
6a)
*/

objekt(a).
objekt(b).
objekt(g(X)):-objekt(X).
objekt(f(X,Y)):-objekt(X),objekt(Y).

/*
6b)
*/


gut(a).
gut(g(X)):-gut(X).
gut(f(X,Y)):-gut(X),gut(Y).

/*
6c)
*/


sehr_gut(a).
sehr_gut(b).
sehr_gut(f(X,Y)):-sehr_gut(X),sehr_gut(Y).

/*
Aufgabenblatt 6
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

/*
 * A11-a)
 * multiset(+Liste) % true if all elements of the list are an atom
 */

multiset([]) :- true. % not sure if required since atom([]) is true.
multiset([X|Liste]) :- atom(X), multiset(Liste).

/*
 * A11-b)
 * element(+Atom,+Multiset) % check if Atom is in Multiset
 */

% A11-b-i)
element(Atom, [Atom|_]) :- atom(Atom), !.
element(Atom, [_|Tail]) :- atom(Atom), element(Atom, Tail).

% A11-b-ii)
element_IFTHENELSE(Atom, [Head|Tail]) :- atom(Atom), (Atom=Head -> true; element_IFTHENELSE(Atom, Tail)).

% A11-c)
% Wenn einmal gefunden mit dem Rest die alte Variante nutzen welche
% bereits alles erfüllt was wir dann noch brauchen
double(Atom, [Head|Tail]) :- atom(Atom), (Atom=Head -> element(Atom, Tail); double(Atom, Tail)).

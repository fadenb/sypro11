/*
Aufgabenblatt 6
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

/*
 * A12-a)
 * Definieren Sie ein zu baum/1 aus Aufgabe ?? ähnliches Prädikat
 * tree/1, für allgemeinere Bäume, bei denen eine Verzweigung in ein,
 * zwei oder mehr Teilbäume erlaubt ist, und bei denen die Blätter
 * Prolog-Atome sein dürfen.
 */

tree([]) :- !.
tree([H|RestTree]) :- !,tree(H),tree(RestTree).
tree(leave(EinAtom)) :- atom(EinAtom),!.
%tree(atom(_)) :- !.
%tree([EinAtom]) :- EinAtom.

/*
 * A12-b)
 * Definieren Sie analog zum Prädikat blaetter(+Baum,-Blattmarken) aus
 * Aufgabe ?? ein Prädikat leaves(+Tree,-Atoms), das die Atome an den
 * Blättern solcher Bäume mit mehreren direkten Teilbäumen ausgibt.
 */
leaves(A,B) :- leavesi2(A,B).

% A12-b-i)
leavesr([],[]) :- !.
leavesr([H|List],As) :-	atom(H), leavesr(List,MoreAs), As=[H|MoreAs]
		      |	leavesr(H,MoreAs),
		      	leavesr(List,MoreMoreAs),
		      	append(MoreAs,MoreMoreAs,As).
leavesr(Atom,[Atom]) :- !, atom(Atom).

% A12-b-ii)
leavesi2(Treelist,Atoms) :- leavesi(Treelist,[],Atoms).
leavesi([],AAkc,AAkc).
leavesi([H|List],AAkc,As) :- atom(H), !, append(AAkc, [H], MoreAAkc),leavesi(List, MoreAAkc, As)
			| 	leavesi(H, [], MoreAAkc),
				append(AAkc, MoreAAkc, MoreMoreAAkc),
				leavesi(List, MoreMoreAAkc, As).

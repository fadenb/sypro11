% a8)
% Keine doppelten Elemente in der Menge da gematched wird ob X am Anfang
% der Menge steht und falls ja nicht nochmals geaddet wird
% Vor diesem Vergleich wird die Menge quasi immer verkürzt und so alle
% Elemente geprüft
toSet([],_).
toSet([H|L],M) :-
	add(H,M,M),toSet(L,M).

add(X,[],[X]).
add(X,[X|IS],[X|IS]) :- !. %ggf nen ! damit nächste regeln nicht greift
add(X,[H|IS],[H|OS]) :- add(X,IS,OS).




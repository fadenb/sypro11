/*
Aufgabenblatt 4
Symbolisches Programmieren WiSe 11/12
Tristan Helmich & Martin Dahse
*/

farbe(rot).
farbe(blau).
farbe(gelb).

fastgleich(L1,L2) :-
	anzahlen(L1,Rote,Blaue,Gelbe),
	anzahlen(L2,Rote,Blaue,Gelbe).


% 7a) rekursiv
anzahlen([X], 1,0,0) :- X = rot, farbe(X).
anzahlen([X], 0,1,0) :- X = blau, farbe(X).
anzahlen([X], 0,0,1) :- X = gelb, farbe(X).
anzahlen([X|RestFarbliste],R,B,G) :-
	farbe(X), X=rot, anzahlen(RestFarbliste,AnzahlRote,B,G), R is AnzahlRote + 1 |
	farbe(X), X=blau, anzahlen(RestFarbliste,R,AnzahlBlaue,G), B is AnzahlBlaue + 1 |
	farbe(X), X=gelb, anzahlen(RestFarbliste,R,B,AnzahlGelbe), G is AnzahlGelbe + 1.

/* 7b) iterativ
Musterlösung würde mich freuen da ich mir nicht sicher bin wo
(abgesehen von mehr Speicherverbrauch der Unterschied liegt)
*/
anzahleni(L,R,B,G) :- anzahleni(L, 0,0,0, R,B,G).
anzahleni([], R,B,G, R,B,G) :- !.
anzahleni([H| T], R,B,G, RN,BN,GN) :- H=rot, !, R1 is R+1, anzahleni(T, R1,B,G, RN,BN,GN).
anzahleni([H| T], R,B,G, RN,BN,GN) :- H=gelb, !, G1 is G+1, anzahleni(T, R,B,G1, RN, BN,GN).
anzahleni([H| T], R,B,G, RN,BN,GN) :- H=blau, !, B1 is B+1, anzahleni(T, R,B1,G, RN,BN,GN).
anzahleni([H| T], R,B,G, RN,BN,GN) :- _=H, anzahleni(T, R,B,G, RN,BN,GN).
